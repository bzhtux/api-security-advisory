from libs import rss
import yaml
import sys
from datetime import date


class Category:
    """ Category class for security advisory """

    def __init__(self):
        self.config = "feedsconfig.yml"

    def get_all(self):
        """ Get all categories from feeds config file
            
            :return: A list of category
            :rtype: list
        """
        results = list()
        with open(self.config) as fd:
            items = yaml.load(fd)
        for cat in items:
            results.append(cat)
        self.categories = results
        return self.categories

class Fetch:
    """ Fetch class for security advisory """

    def __init__(self, cat):
        # need category to fetch or all for all categories
        self.category = cat
        # Open feeds config file
        with open("feedsconfig.yml") as fd:
            self.feeds = yaml.load(fd)
        # Start feed object
        self.feed = rss.Feed()

    def run(self):
        """ Run a feed fetch from origin server

            :return: A dictionary with title, updated date and link
            :rtype: dict
        """
        today_tmp = date.today()
        today = today_tmp.strftime('%Y-%m-%d')
        results = dict()
        if self.category == "all":
            for item in self.feeds:
                rss = self.feed.get(feed_urls[item])
                for entry in rss.entries:
                    try:
                        results[entry.title] = {'updated': entry.updated, 'link': entry.link}
                    except AttributeError:
                        results[entry.title] = {'updated': today, 'link': entry.link}
        else:
            rss = self.feed.get(self.feeds[self.category])
        for entry in rss.entries:
            try:
                results[entry.title] = {'updated': entry.updated, 'link': entry.link}
            except AttributeError:
                results[entry.title] = {'updated': today, 'link': entry.link}
        return results
