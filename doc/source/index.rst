.. Sosadd Security Advisory documentation master file, created by
   sphinx-quickstart on Mon Oct 31 21:51:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sosadd Security Advisory's documentation!
====================================================

Contents:

.. toctree::
   :maxdepth: 2

   dev.rst
   user.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

