Developper guide
================

security-advisory is Python Flask application providing an API to fetch security feeds.


.. automodule:: app
    :members:

.. automodule:: sa
    :members:
