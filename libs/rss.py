import feedparser


class Feed:

    def __init__(self):
        pass

    def get(self, rss_link):
        """ Get a feed from rss_link

            :param rss_link: rss link to fetch
            :type rss_link: sring
            :return: Feedparser dict if get end successfully, an empty dict if not
            :rtype: Feedparser dict
        """
        rss = dict()
        try:
            rss = feedparser.parse(rss_link)
            return rss
        except Exception as e:
            #print "get: {0}".format(e)
            return rss
