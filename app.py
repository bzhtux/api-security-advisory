from sa import Category, Fetch
#from flask.ext.api import FlaskAPI
from flask_api import FlaskAPI


app = FlaskAPI(__name__)


@app.route('/', methods=['GET'])
def api_redirect_home():
    """ Redirect / to help page with status_code = 301"""
    return {}, 301, {'Location': '/api/help'}


@app.route('/api/help', methods=['GET'])
def api_help():
    """ Provide a short help to start with API """
    help_message = {'/api/categories' : 'Provides all available categories',
                    '/api/rss/<category>' : 'Provide feeds for <category'
                   }
    return help_message

@app.route('/api/categories', methods=['GET'])
def api_get_all_categories():
    """ Provide all available categories

        :return: a list with each category
        :rtype: json
    """
    cats = Category()
    return cats.get_all()


@app.route('/api/rss/<category>', methods=['GET'])
def api_get_rss(category):
    """ Provide feeds for given category 
    
        :param category: a category from /api/categories
        :type category: string
        :return: dict of all available feeds for given category
        :rtype: json
    """
    rss = Fetch(category)
    return rss.run()


if __name__ == "__main__":
    app.run(debug=True)
