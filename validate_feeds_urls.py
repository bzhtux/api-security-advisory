import yaml
import feedparser


class Validate:

    def __init__(self, config_file):
        self.config = config_file

    def validate_config(self):
        results = dict()
        with open(self.config) as fd:
            items = yaml.load(fd)
        for cat in items:
            req = feedparser.parse(items[cat])
            if req.status == 200:
                results[cat] = items[cat]
        return results


if __name__ == "__main__":
    v = Validate("feedsconfig.yml")
    validate_conf = v.validate_config()
    print validate_conf
    for i in validate_conf:
        print "%s : '%s'" % (i, validate_conf[i])
